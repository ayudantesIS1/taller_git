# GIT Cheat Sheet

Git is the most popular **open source distributed version control system**. This cheat sheet summarizes commonly used Git command line instructions for quick reference.


# Configure Tooling

Configure user information for all local repositories.

## $ git config --global user.name "[name]"

Sets the name you want atached to your commit transactions.

## $ git config --global user.email "[email address]"

Sets the email you want atached to your commit transactions.

## $ git config --global color.ui auto

Enables helpful colorization of command line output.


# Create repositories

Start a new repository or obtain one from an existing URL.

## $ git init [project-name]

Creates a new local repository with the specified name.

## $ git clone [url]

Downloads a project and its entire version history.

## Make Changes

Review edits and craf a commit transaction.


## $ git status

Lists all new or modified files to be commited.

## $ git add [file]

Snapshots the file in preparation for versioning.


## $ git commit -m "[descriptive message]"

Records file snapshots permanently in version history.

## Synchronize Changes

Register a repository bookmark and exchange version history.

## $ git push [alias] [branch]

Uploads all local branch commits to remote repository.

## $ git pull

Downloads bookmark history and incorporates changes.